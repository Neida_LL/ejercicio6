import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { HomeComponent } from './components/home/home.component';
import { Info1Component } from './components/info1/info1.component';
import { Info2Component } from './components/info2/info2.component';
import { Info3Component } from './components/info3/info3.component';
import { Info4Component } from './components/info4/info4.component';
import { InicioComponent } from './components/inicio/inicio.component';

const routes: Routes = [
  {path:'home',component: HomeComponent},
  {path:'about',component: AboutComponent},
  {path:'contact',component: ContactComponent },
  {path:'inicio',component: InicioComponent },
  {path:'info1',component: Info1Component },
  {path:'info2',component: Info2Component },
  {path:'info3',component: Info3Component },
  {path:'info4',component: Info4Component },
  {path:'**',pathMatch:'full', redirectTo:'home'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
